
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.emids.healthCare.Interface.CurrentHabit;
import com.emids.healthCare.Interface.CurrentHealth;
import com.emids.healthCare.Interface.Gender;
import com.emids.healthCare.business.HealthCareBusiness;
import com.emids.healthCare.entity.Alcohol;
import com.emids.healthCare.entity.BloodPressure;
import com.emids.healthCare.entity.BloodSugar;
import com.emids.healthCare.entity.DailyExercise;
import com.emids.healthCare.entity.Drugs;
import com.emids.healthCare.entity.Hypertension;
import com.emids.healthCare.entity.Male;
import com.emids.healthCare.entity.Overweight;
import com.emids.healthCare.entity.Person;
import com.emids.healthCare.entity.Smoking;

class HealthCareJTest {
	
	private static final Double BASE_PREMIUM = 5000D;

	@Test
	void test() {
		List<CurrentHealth> healthList = new ArrayList<>();
		healthList.add(new BloodPressure());
		healthList.add(new BloodSugar());
		healthList.add(new Hypertension());
		healthList.add(new Overweight());
		
		List<CurrentHabit> habitList = new ArrayList<>();
		habitList.add(new Alcohol());
		habitList.add(new DailyExercise());
		habitList.add(new Drugs());
		habitList.add(new Smoking());
		
		Gender gender = new Male();
		
		Person person = new Person("Sandipt", gender, 34, healthList, habitList);
		
		HealthCareBusiness business = new HealthCareBusiness();
		Double premium = business.getTotalPremium(person);
		
		assertEquals(Double.valueOf(7293.0), Double.valueOf(premium));
		
		//fail("Not yet implemented");
	}

}
