package com.emids.healthCare.Interface;

public interface CurrentHabit {

	public String getName();
	
	public Boolean isAffected();
	
	public Boolean isGoodHabit();
	
	public default Double getPremiumPercentage() {
		if(isAffected()) {
			if(isGoodHabit()) {
				return 0.03;
			}else {
				return -0.03;
			}
		}
		return 0D;
	}
}
