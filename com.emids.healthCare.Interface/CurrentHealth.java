package com.emids.healthCare.Interface;

public interface CurrentHealth {

	public String getName();
	
	public Boolean isAffected();
	
	public default Double getPremiumPercentage() {
		if(isAffected()) {
			return 0.01;
		}
		return 0D;
	}
}
