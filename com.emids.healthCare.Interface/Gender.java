package com.emids.healthCare.Interface;

public interface Gender {
	
	public String gender();
	
	public default Double getPremiumPercentage() {
		switch (gender()) {
		case "Male":
			return 0.02;
		case "Female":
			return 0.00;
		case "Other":
			return 0.00;

		}
		return 0D;
	}

}
