package com.emids.healthCare.business;

import com.emids.healthCare.Interface.CurrentHabit;
import com.emids.healthCare.Interface.CurrentHealth;
import com.emids.healthCare.entity.Person;

public class HealthCareBusiness {
	private static final Double BASE_PREMIUM = 5000D;
	
	public Double getTotalPremium(Person person) {
		Double totalPremium = 0D;
		totalPremium = getBasePremium(person);
		Double genderFactor = getGenderPremium(person);
		totalPremium = totalPremium *(1 + genderFactor);
		Double preExistingPremium = getPreExistingPremium(person);
		totalPremium = totalPremium * (1 + preExistingPremium);
		Double habitsFactor = getHabitsPremium(person);
		totalPremium = totalPremium * (1 + habitsFactor);
		
		return totalPremium;
	}
	
	public Double getHabitsPremium(Person person) {
		if(person == null || person.getHabitList() == null || person.getHabitList().size() == 0) {
			return 0D;
		}
		Double increasedValue = 0d;
		for(CurrentHabit habit: person.getHabitList()) {
			increasedValue = increasedValue + habit.getPremiumPercentage();
		}
		return increasedValue;
	}
	
	public Double getPreExistingPremium(Person person) {
		if(person == null || person.getCurrentHealthList() == null || person.getCurrentHealthList().size() == 0) {
			return 0D;
		}
		
		Double increasedValue = 0d;
		for(CurrentHealth health : person.getCurrentHealthList()) {
				increasedValue = increasedValue + health.getPremiumPercentage(); 
		}
		return increasedValue;
	}
	
	public Double getGenderPremium(Person person) {
		if(person == null || person.getGender() == null || person.getGender().gender().equals("")) {
			return 0D;
		}
		return person.getGender().getPremiumPercentage();
	}
	
	public Double getBasePremium(Person person) {
		if(person == null || person.getAge() <= 0) {
			return 0D;
		}
		if(person.getAge() <= 18) {
			return BASE_PREMIUM;
		}else if(18 < person.getAge() && person.getAge() <= 25) {
			Double total = BASE_PREMIUM*110/100;
			return total;
		}else if(25 < person.getAge() && person.getAge() <= 30) {
			Double total = BASE_PREMIUM*120/100;
			return total; 
		}else if(30 < person.getAge() && person.getAge() <= 35) {
			Double total = BASE_PREMIUM*130/100;
			return total; 
		}else if(35 < person.getAge() && person.getAge() <= 40) {
			Double total = BASE_PREMIUM*140/100;
			return total; 
		}else if(person.getAge() > 40) {
			Double total = BASE_PREMIUM*140/100;
			Double ageDiff = (double)person.getAge() - 40;
			Double noOfAgeSlab = ageDiff / 5;
			total = total + BASE_PREMIUM * noOfAgeSlab * 20 / 100;
			return total;
		}
		return 0D;
}
}
