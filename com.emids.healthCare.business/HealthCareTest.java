package com.emids.healthCare.business;

import java.util.ArrayList;
import java.util.List;

import com.emids.healthCare.Interface.CurrentHabit;
import com.emids.healthCare.Interface.CurrentHealth;
import com.emids.healthCare.Interface.Gender;
import com.emids.healthCare.entity.Alcohol;
import com.emids.healthCare.entity.BloodPressure;
import com.emids.healthCare.entity.BloodSugar;
import com.emids.healthCare.entity.DailyExercise;
import com.emids.healthCare.entity.Drugs;
import com.emids.healthCare.entity.Habits;
import com.emids.healthCare.entity.Health;
import com.emids.healthCare.entity.Hypertension;
import com.emids.healthCare.entity.Male;
import com.emids.healthCare.entity.Overweight;
import com.emids.healthCare.entity.Person;
import com.emids.healthCare.entity.Smoking;

public class HealthCareTest {
	
	public static void main(String[] args) {
		
		List<CurrentHealth> healthList = new ArrayList<>();
		healthList.add(new BloodPressure());
		healthList.add(new BloodSugar());
		healthList.add(new Hypertension());
		healthList.add(new Overweight());
		
		/*Health health1 = new Health("Hypertension", false);
		Health health2 = new Health("Blood pressure", false);
		Health health3 = new Health("Blood sugar", false);
		Health health4 = new Health("Overweight", true);
		healthList.add(health1);
		healthList.add(health2);
		healthList.add(health3);
		healthList.add(health4);
		*/
		
		
		
		List<CurrentHabit> habitList = new ArrayList<>();
		habitList.add(new Alcohol());
		habitList.add(new DailyExercise());
		habitList.add(new Drugs());
		habitList.add(new Smoking());
		
		/*Habits habit1 = new Habits("Smoking", false, false);
		Habits habit2 = new Habits("Alcohol", true, false);
		Habits habit3 = new Habits("Daily exercise", true, true);
		Habits habit4 = new Habits("Drugs", false, false);
		
		habitList.add(habit1);
		habitList.add(habit2);
		habitList.add(habit3);
		habitList.add(habit4);
		*/
		
		Gender gender = new Male();
		
		Person person = new Person("Sandipt", gender, 34, healthList, habitList);
		
		HealthCareBusiness business = new HealthCareBusiness();
		Double premium = business.getTotalPremium(person);
		System.out.println(premium);
		
		
	}

}
