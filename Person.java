package com.emids.healthCare.entity;

import java.util.List;

import com.emids.healthCare.Interface.CurrentHabit;
import com.emids.healthCare.Interface.CurrentHealth;
import com.emids.healthCare.Interface.Gender;

public class Person {
	
	private String name;
	private Gender gender;
	private Integer age;
	private List<CurrentHealth> currentHealthList;
	private List<CurrentHabit> habitList;
	
	public Person() {
		
	}
	
	public Person(String name, Gender gender, Integer age, List<CurrentHealth> currentHealthList, List<CurrentHabit> habitList) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.currentHealthList = currentHealthList;
		this.habitList = habitList;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public List<CurrentHealth> getCurrentHealthList() {
		return currentHealthList;
	}
	public void setCurrentHealthList(List<CurrentHealth> currentHealthList) {
		this.currentHealthList = currentHealthList;
	}
	public List<CurrentHabit> getHabitList() {
		return habitList;
	}
	public void setHabitList(List<CurrentHabit> habitList) {
		this.habitList = habitList;
	}
	
	

}
