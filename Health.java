package com.emids.healthCare.entity;

public class Health {
	private String name;
	private Boolean isAffected;
	
	public Health(String name, Boolean isAffected) {
		super();
		this.name = name;
		this.isAffected = isAffected;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsAffected() {
		return isAffected;
	}

	public void setIsAffected(Boolean isAffected) {
		this.isAffected = isAffected;
	}
	
	

}
