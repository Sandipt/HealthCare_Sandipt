package com.emids.healthCare.entity;

public class Habits {
	private String name;
	private Boolean isAffected;
	private Boolean isGoodHabit;
	
	public Habits(String name, Boolean isAffected, Boolean isGoodHabit) {
		super();
		this.name = name;
		this.isAffected = isAffected;
		this.isGoodHabit = isGoodHabit;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsAffected() {
		return isAffected;
	}

	public void setIsAffected(Boolean isAffected) {
		this.isAffected = isAffected;
	}
	
	public Boolean getIsGoodHabit() {
		return isGoodHabit;
	}
	
	public void setIsGoodHabit(Boolean isGoodHabit) {
		this.isGoodHabit = isGoodHabit;
	}
	

}
